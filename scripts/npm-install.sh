#!/bin/bash
cd /var/www/esgrown
sudo npm install 
sudo npm install --unsafe-perm node-sass
sudo cp /var/env/.env.production /var/www/esgrown
sudo npm run-script build
cd /var/www/esgrown/backend
sudo npm install
sudo cp /var/env/.env /var/www/esgrown/backend