#!/bin/bash
cd /var/www
if [ -d "esgrown" ]; then
sudo rm -R esgrown
fi
cd /opt/codedeploy-agent/deployment-root/deployment-instructions/
if [ -e "*-cleanup" ]; then
sudo rm *-cleanup
fi
