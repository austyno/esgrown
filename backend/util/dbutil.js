const mongoose = require("mongoose");

const initClientDbConnection = async () => {

    let DB_URL
    if (process.env.NODE_ENV === 'development') {
        DB_URL = `${'mongodb://localhost:27017/test?retryWrites=true&w=majority'}`
    } else {
        DB_URL = process.env.MAIN_DB_URI;
    }

    try {
        const db = await mongoose.connect(DB_URL, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
        if (db) {
            console.log("client MongoDB Connection ok!");
            return db;
        }
    } catch (e) {
        console.log("MongoDB Connection Error");
    }
};

module.exports = {
    initClientDbConnection
};