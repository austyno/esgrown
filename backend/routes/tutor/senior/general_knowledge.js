const router = require('express').Router();
const verifyToken = require('../../../validation/authenticate_token');
const GeneralKnowledge = require('../../../models/tutor/senior/general_knowledge.model');

router.post(`/add`, verifyToken,  (req, res) => {
    const title = req.body.title;
    const content = req.body.content;
    const media = req.body.media;
    const subject = req.body.subject;
    const tutor_id = req.body.tutor_id;

    const tutorContent = new GeneralKnowledge({ tutor_id,subject,title, content, media });
    tutorContent.save().then(generalKnowledge => res.json(generalKnowledge)).catch(err => res.status(400).json(err))
});

router.get(`/:id`, verifyToken, (req, res) => {
    GeneralKnowledge.find({ tutor_id: req.params.id })
        .then(content => res.json(content))
        .catch(err => res.status(400).json(err))
})

module.exports = router;