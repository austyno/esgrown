const router = require('express').Router();
const Maths = require('../../../models/tutor/senior/maths.model');
const verifyToken = require('../../../validation/authenticate_token');


router.post(`/add`, verifyToken, (req, res) => {
    const title = req.body.title;
    const content = req.body.content;
    const media = req.body.media;
    const subject = req.body.subject;
    const tutor_id = req.body.tutor_id;

    const tutorContent = new Maths({ tutor_id, subject, title, content, media });
    tutorContent.save()
        .then(maths => res.json(maths))
        .catch(err => res.status(400).json(err))
});

router.get(`/:id`, verifyToken, (req, res) => {
    Maths.find({ tutor_id: req.params.id })
        .then(content => res.json(content))
        .catch(err => res.status(400).json(err))
})


module.exports = router;