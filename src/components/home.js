import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import { AuthContext } from "../AuthContext";
import NavBar from './navbar';
import toast from '../util/toast';
import '../img/bg1.jpg';
import TestimonialSlider from './reusable/testimonial_slider/testimonial_slider';
import '../App.css';
import Card from './reusable/home_card/card';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import { Container, Col, Row, Form, Button, Jumbotron, Popover, OverlayTrigger } from "react-bootstrap";

import student from '../img/Student_Education_Advisory.jpg';
import teacher from '../img/businesswoman-4126489_1280.jpg';
import school from '../img/Education_Advisory.jpg';
import company from '../img/woman-2773007_1280.jpg';
import applicant from '../img/mediensturmer-aWf7mjwwJJo-unsplash.jpg';
import employee from '../img/Personnel_Competence_Management.jpg';


export default class Home extends Component {

    constructor(props) {
        super(props);

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        // this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            email: '',
            password: '',
            user: [{ name: "", islogged: false, email: "" }],
            loading: false
        }
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        });
    }

    setLoading() {
        this.setState({
            loading: true
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    notify(message, type) {
        return toast(message, type);
    }

    render() {
        //Styles
        const styles = {
            'background-color': '#DCDCDC',
            'opacity': '.5',
            'font-family': 'quicksand',
            'color': 'black',
        };
        const formStyle = {
            'border': '1px solid',
            'border-radius': '5px',
            'background-color': 'transparent',
            'opacity': '0.8',
            'color': 'white',
        };
        var settings = {
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            centerMode: true,
        };

        // Popover's rendering
        const popover = (
            <Popover id="popover-basic">
                <Popover.Title as="h3" style={{ 'font-family': 'gothic' }}>Which account type do want to create?</Popover.Title>
                <Popover.Content>
                    <Row>
                        <Col>
                            <Link to="/signup_individual">
                                <Button variant="info" className="btn-sm">Individual</Button>
                            </Link>
                        </Col>
                        <Col>
                            <Link to="/signup_corporate">
                                <Button variant="info" className="btn-sm">Corporate</Button>
                            </Link>
                        </Col>
                    </Row>
                </Popover.Content>
            </Popover>
        );



        return (
            <AuthContext.Consumer>
                {
                    (context) => (
                        <div>
                            <div className="bg">
                                <NavBar />
                                <Container >

                                    <Row>
                                        <Col className="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12" >
                                            <div className="d-flex align-items-end justify-content-end mt-3" style={{ height: '100%' }}>
                                                <h2 style={{ color: 'white', fontSize: '40px', fontFamily: 'quicksand', }}>Creating Synergy between Education and Industry</h2>
                                            </div>
                                        </Col>
                                        <br />
                                        <Col className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></Col>
                                        <br />
                                        <Col className="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                                            <Container style={formStyle} >
                                                <br />
                                                <label style={{ 'fontWeight': 'bold', fontFamily: 'quicksand', fontSize: '25px' }}>SignIn to your Account</label>
                                                <hr />
                                                <Form >
                                                    <Form.Group controlId="formBasicEmail">
                                                        <Form.Label style={{ fontFamily: 'quicksand' }}>Email address:</Form.Label>
                                                        <Form.Control type="email" required placeholder="Enter email" onChange={this.onChangeEmail} value={this.state.email} />
                                                            We'll never share your email with anyone else.
                                                        <Form.Text className="text-muted" style={{ color: 'white' }}>
                                                        </Form.Text>
                                                    </Form.Group>
                                                    <Form.Group controlId="formBasicPassword">
                                                        <Form.Label>Password:</Form.Label>
                                                        <Form.Control type="password" required placeholder="Password" onChange={this.onChangePassword} value={this.state.password} />
                                                    </Form.Group>
                                                    <Button type="submit" className="btn_submit mb-2" disabled={this.state.loading} onClick={
                                                        (e) => {
                                                            e.preventDefault();
                                                            const User = {
                                                                email: this.state.email,
                                                                password: this.state.password
                                                            }
                                                            // ==========================================
                                                            //Make a post to the api route for login
                                                            try {

                                                                this.setState({ loading: true });
                                                                axios.post(`${process.env.REACT_APP_API_ENDPOINT}/individuals/login`, User)
                                                                    .then(res => {
                                                                        // debugger

                                                                        if (Object.keys(res.data).length > 0) {

                                                                            if (res.data.corp) {
                                                                                console.log(res.data.corp[0])
                                                                                // Register the Corporate User in Session storage
                                                                                const Global_CorpUser = {
                                                                                    isLogged: true,
                                                                                    id: res.data.corp[0]._id,
                                                                                    name: res.data.corp[0].org_name,
                                                                                    email: res.data.corp[0].email,
                                                                                    phone: res.data.corp[0].phone,
                                                                                    doi: res.data.corp[0].doi,
                                                                                    country: res.data.corp[0].country,
                                                                                    state: res.data.corp[0].state,
                                                                                    status: res.data.corp[0].status,
                                                                                    org_type: res.data.corp[0].org_type,
                                                                                    sub_status_rm: res.data.corp[0].sub_status_rm,
                                                                                    token: res.data.token

                                                                                }
                                                                                // Save corp data to session Storage
                                                                                sessionStorage.setItem("key", JSON.stringify(Global_CorpUser));
                                                                                console.log(JSON.parse(sessionStorage.getItem("key")));
                                                                                this.setState({
                                                                                    email: '',
                                                                                    password: ''
                                                                                });
                                                                                this.setState({ loading: false });

                                                                                //UPDATE COMPONENT USER STATE HERE AND NAVIGATE TO THE DASHBOARD                                              
                                                                                context.setUserAuthData(true);

                                                                                //UPDATE COMPONENT USER STATE HERE AND NAVIGATE TO THE DASHBOARD                                              
                                                                                context.setUserAuthData(true);
                                                                                window.location = "/frontier";
                                                                                
                                                                                // this.props.history.push('/frontier');
                                                                                // extra comment


                                                                            }

                                                                           

                                                                            if (res.data.ind) {
                                                                                // Register the individual user in session storage
                                                                                const GlobalUser = {
                                                                                    isLogged: true,
                                                                                    id: res.data.ind[0]._id,
                                                                                    email: res.data.ind[0].email,
                                                                                    name: res.data.ind[0].fullname,
                                                                                    phone: res.data.ind[0].phone,
                                                                                    gender: res.data.ind[0].gender,
                                                                                    dos: res.data.ind[0].dob,
                                                                                    country: res.data.ind[0].country,
                                                                                    state: res.data.ind[0].state,
                                                                                    status: res.data.ind[0].status,
                                                                                    lastLogin: res.data.ind[0].lastLogin,
                                                                                    org_type: res.data.ind[0].org_type,
                                                                                    sub_status_eas: res.data.ind[0].sub_status_eas,
                                                                                    sub_status_efa: res.data.ind[0].sub_status_efa,
                                                                                    sub_status_lm: res.data.ind[0].sub_status_lm,
                                                                                    sub_status_rm: res.data.ind[0].sub_status_rm,
                                                                                    sub_status_compt_mgt: res.data.ind[0].sub_status_compt_mgt,
                                                                                    tic: res.data.ind[0].tic,
                                                                                    jobs: res.data.ind[0].jobs,
                                                                                    token: res.data.token
                                                                                }

                                                                                // Save Individual data to session Storage
                                                                                sessionStorage.setItem("key", JSON.stringify(GlobalUser));
                                                                                console.log(JSON.parse(sessionStorage.getItem("key")));
                                                                                this.setState({
                                                                                    email: '',
                                                                                    password: ''
                                                                                });
                                                                                // window.location = "/frontier";
                                                                                // this.props.history.push('/frontier');
                                                                                context.setUserAuthData(true);
                                                                                window.location = "/frontier";

                                                                            }


                                                                            this.setState({ loading: false })
                                                                        } else {
                                                                            console.log("data not found");
                                                                            this.notify("Email or Password is wrong", "error");
                                                                            this.setState({ loading: false })
                                                                        }
                                                                    })

                                                            } catch (e) {
                                                                console.log(e);
                                                                this.setState({ loading: true });
                                                            }
                                                            // .catch(err => console.log("Error here is: " + err));

                                                            // =======================
                                                        }

                                                    }>
                                                        Login
                                                           {this.state.loading ? <i className="fa fa-spinner fa-spin ml-2"></i> : ""}
                                                    </Button>

                                                    <OverlayTrigger trigger="click" placement="left" overlay={popover}>
                                                        {/* variant="primary btn-sm" */}
                                                        <Button className="btn_submit mb-4" >Create an Esgrown Account</Button>
                                                    </OverlayTrigger>
                                                    <br />
                                                    <label>Forgot password? <Link to="/forgotPassword">Reset</Link></label>
                                                </Form>
                                            </Container>
                                        </Col>
                                    </Row>
                                    <br />
                                </Container>
                            </div>

                            {/* Bottom Page */}
                            <Container className="mt-5">
                                <Row>
                                    <Col className="col-lg-6">
                                        <Container>
                                            <Jumbotron className="jumbo" style={styles} >
                                                <p style={{ fontFamily: 'quicksand', fontSize: '16px', textAlign: 'justify' }}>
                                                    In serving your individual and corporate user needs as a student, teacher, school, applicant, company, employee or business manager, we help you to:
                                                     <hr />
                                                    align your education to industry knowledge and skills needs; access financing for education through grants and scholarships; connect with industry and join the workforce of a company where knowledge and skills you have acquired through your education are needed for productivity; build workforce that meets your industry knowledge and skill needs for business, and optimize the effectiveness of manpower in leveraging knowledge and skills acquired through education into delivery of business goals.  We keep you up to speed on principles for high performance, effective leadership and good management of your organization.
                                                    <hr />

                                                </p>
                                                <h3 data-aos="zoom-in-right">Welcome to ESGROWN</h3>
                                            </Jumbotron>
                                        </Container>
                                    </Col>
                                    <Col className="col-lg-6">
                                        <div>
                                            <div className="d-flex justify-content-center align-items-center">
                                                <h4 style={{ fontFamily: 'quicksand', }}>Built For</h4>
                                            </div>
                                            <hr />
                                            <div >
                                                <Carousel>
                                                    <div >
                                                        <img src={teacher} alt="" />
                                                        <p className="legend">Teachers</p>
                                                    </div>
                                                    <div>
                                                        <img src={student} alt="" />
                                                        <p className="legend">Students</p>

                                                    </div>
                                                    <div >
                                                        <img src={school} alt="" />
                                                        <p className="legend">Schools</p>

                                                    </div>
                                                    <div >
                                                        <img src={applicant} alt="" />
                                                        <p className="legend">Applicants</p>

                                                    </div>
                                                    <div >
                                                        <img src={company} alt="" />
                                                        <p className="legend">Companies</p>


                                                    </div>
                                                    <div>
                                                        <img src={employee} alt="" />
                                                        <p className="legend">Employees</p>

                                                    </div>
                                                </Carousel>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>


                            {/* Bottom Slider */}
                            <div className="mt-4">
                                <TestimonialSlider />
                            </div>


                            {/* Footter */}
                            <div className="footer">
                                <div className="d-flex justify-content-center align-items-center">
                                    <div className="mt-2">Copyright &#169; 2020 - Brain Wealth Projects Inc.</div>
                                </div>
                            </div>
                        </div>
                    )
                }
            </AuthContext.Consumer>
        );
    }
}