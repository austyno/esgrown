import React from 'react';
import '../App.css';


const SideBar = () => {

    return (
        <>
            <div className="container sidebar mt-1">
                <div className="row">
                    <div className="col"><p className="text">Our unique mission is to bridge the divides that are based on strong education that equips people with capability to create value and the competencies of people in leveraging knowledge and skills through learning into actual creation of value or lack of it.</p></div>
                </div>
                <hr />
                <div className="col row terms mt-3">
                    <p style={{ fontFamily: 'cursive', fontSize: '' }}>From</p>
                    <p style={{ fontSize: '', color: '#21a5e7', fontFamily: 'sanserif', fontWeight: '', textAlign: 'center' }}>Brain Wealth Projects</p>
                    <div><p style={{ fontSize: '12px' }}> &copy;2020. All Rights Reserved</p></div>
                    <div>Terms & Policy FeedBack</div>
                </div>
            </div>


        </>
    );

}
export default SideBar;