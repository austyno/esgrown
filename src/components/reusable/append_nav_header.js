import React from 'react';

const AppendNavHeader = ({title}) => {
    return (
        <>
            <div className="d-flex justify-content-center align-items-center p-4" 
                style={{marginTop:'62px', width:'100vw', height:'30px', backgroundColor:'#F5F5F5', fontSize:'25px', fontFamily:'quicksand' }}>
                {title}
            </div>
        </>
    );
}

export default AppendNavHeader;