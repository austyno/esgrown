import React, { useEffect, useState } from 'react';
import SideBar from '../sidebar';
import '../../App';
import { Card, CardActionArea, CardActions, CardContent, Typography } from '@material-ui/core';
import AppendNavHeader from '../reusable/append_nav_header';
import '../../App.css'


export default function Account({ subrm, }) {

  const [company, setCompany] = useState('');
  const [displayRmMore, setDisplayRmMore] = useState('none')
  const [rmSpanDisplay, setRmSpanDisplay] = useState('inline-block')
  const [displayLmMore, setDisplayLmMore] = useState('none')
  const [lmSpanDisplay, setLmSpanDisplay] = useState('inline-block')

  useEffect(() => {
    const user = JSON.parse(sessionStorage.getItem('key'));
    setCompany(user)
  }, [])

  const rmReadMore = () => {
    setDisplayRmMore('inline-block');
    setRmSpanDisplay('none')
  }

  const displayLessRm = () => {
    setDisplayRmMore('none');
    setRmSpanDisplay('inline-block')
  }

  const lmReadMore = () => {
    setDisplayLmMore('inline-block');
    setLmSpanDisplay('none')
  }

  const displayLessLm = () => {
    setDisplayLmMore('none');
    setLmSpanDisplay('inline-block')
  }

  return (
    <>
      <div >
        <AppendNavHeader title="" />
        <div className="row" style={{ marginTop: '0px' }}>
          <div className="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12 col-12 sid" style={{ marginLeft: '0px', marginRight: '0px' }}>
            {/* <div className=""> */}
            <div class="jumbotron image" style={{ height: '60%', marginLeft: '15px' }}>
              <div className="d-flex flex-column justify-content-between p-4" >
                <div className="">
                  <h1 class="" style={{ fontFamily: 'quicksand', color: 'white', fontSize: '25' }}>Welcome to Esgrown</h1>
                  <hr />
                </div>
                <div className=" overlay p-3" style={{ marginTop: '0px' }}>
                  <p class="lead d-flex flex-direction-column align-items-center" style={{ color: 'white', fontSize: '20px', fontFamily: 'quicksand', textAlign: 'left' }}>We inspire and empower you to live your highest dreams in the contexts of learning, <br /> productivity and pursuit of success and economic welfare.</p>
                </div>
              </div>
            </div>
            {/* sub cards */}
            <div className="col col-lg-4 col-sm-6 ml-5 mb-5 corp-act-page-border">
              <br />
              <Card className="corp-act-page">
                <CardActionArea className="">
                  <CardContent>
                    <Typography gutterBottom variant="" component="h5">
                      {company && company.org_type === 'school' ? 'Teachers and none-academic staff recruitment management Service' : 'Staff Recruitment Management Service'}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                      <p style={{ fontSize: '16px' }}>We connect you to manpower that with the knowledge and skills that meet your operation needs. We training them. We provided them exercise
                                        <span style={{ color: '#1C8496', display: rmSpanDisplay, fontFamily: 'quicksand', fontWeight: 'bolder' }} onClick={rmReadMore}>...More</span> <span style={{ display: displayRmMore }}> and show you where each prospective personnel you may wish to hire ranks in pre-engagement evaluation of competencies that your knowledge and skills for functional and quality education and effective school administration.<br /><span style={{ color: '#1C8496', fontFamily: 'quicksand', fontWeight: 'bolder' }} onClick={displayLessRm}>Less</span></span></p>
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <button
                    className="btn mt-3 py-2 border-0"
                    style={{ background: '#21A5E7', color: 'white' }}
                    onClick={() => subrm()}
                  >
                    Subscribe
                    </button>
                </CardActions>
              </Card>
            </div>

          </div>
          <div className="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12">
            <SideBar />
          </div>
        </div>
      </div>
    </>
  );
}
