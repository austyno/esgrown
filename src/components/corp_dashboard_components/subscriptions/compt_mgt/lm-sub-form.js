import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Spinner } from 'react-bootstrap';
import PayStackButton from '../../../ind_dashboard_components/paystack/paystackpaymentbutton';
import StaffModal from '../components/all-staff';
import toast from '../../../../util/toast';





const LMSubForm = ({ show, onHide, closeModal }) => {

    const [user, setUser] = useState();
    const [amount, setAmount] = useState();
    const [plan, setPlan] = useState();
    const [payModal, setPayModal] = useState(false);
    const [spinner, setSpinner] = useState(false)
    const [staff, setStaff] = useState([]);
    const [staffModal, setStaffModal] = useState(false)
    const [total, setTotal] = useState();
    const [subPeriod, setSubPeriod] = useState();
    const [Btn, setBtn] = useState(true);
    // const [showAdvisor, setShowAdvisor] = useState(false);
    // const [aic, setAic] = useState('');


    // const [state, setState] = useState({
    //     companyName: '',
    //     email: '',
    //     doi: ''
    // });

    useEffect(() => {
        const userr = JSON.parse(sessionStorage.getItem('key'));
        setUser(userr)


        axios.get(`${process.env.REACT_APP_API_ENDPOINT}/individuals/staff/${userr.id}`, { headers: { 'authorization': `Bearer ${userr.token}` } })
            .then(res => {
                const unsubscribedStaff = res.data.filter(staff => {
                    return staff.sub_status_compt_mgt === 'inactive' || staff.sub_status_compt_mgt === 'completed';
                })
                setStaff(unsubscribedStaff);
            }).catch(err => console.log(err));

    }, []);


    const onChangePlan = (e) => {
        const plan = e.target.value.split('/');
        setAmount(plan[1]);
        setPlan(plan[0]);
        if (Boolean(e.target.value)) {
            setBtn(false)
        } else {
            setBtn(true)

        }
    }

    // const onChangeAdvisor = (e) => {
    //     const option = e.target.value;
    //     if(option == 'yes') setShowAdvisor(true);
    //     if(option == 'no') setShowAdvisor(false);
    // }

    // const onChangeAIC = (e) => {
    //     setAic(e.target.value)
    // }

    const showStaff = () => {
        setStaffModal(true);
        closeModal();
    }

    //get total amount * number of staff and sub
    const closeStaffModal = (plantotal) => {
        setStaffModal(false);
        setTotal(plantotal);
        setPayModal(true);

    }


    //payment functions
    const close = () => {
        console.log("Payment closed");
        setPayModal(false)
    }

    const onSuccess = (res) => {
        setPayModal(false);

        const data = {
            sub_status_compt_mgt: 'active',
        }

        console.log(staff)

        //update substatus of paid staff
        staff.map(st => {
            axios.post(`${process.env.REACT_APP_API_ENDPOINT}/individuals/update/substatus/${st._id}`, data, { headers: { 'authorization': `Bearer ${user.token}` } })
                .then(res => console.log(res.data))
                .catch(err => console.log(err));

            axios.post(`${process.env.REACT_APP_API_ENDPOINT}/competence/management/add`, {
                user_id: st._id,
                company_id: user.id,
                org_type: user.org_type,
                sub_status: 'active',
                user_name: st.fullname,
                user_email: st.email,
                start_date: Date.now(),
                end_date: (86400 * plan) + Date.now()

            }, { headers: { 'authorization': `Bearer ${user.token}` } })
                .then(res => console.log(res.data))
                .catch(err => console.log(err))
        })

        toast(`${staff.length}  subscriptions successful`, 'success');
    }


    return (
        <>
            <Modal show={show} onHide={onHide} centered>
                <Modal.Header>
                    <span style={{ fontSize: '18px' }}>Corporate Subscription</span>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        {/* <div className="row mt-3">
                            <div className="col">
                                <label style={{ fontWeight: 'bold' }}> Company Name</label>
                                <input type="text" name="companyName" value={state.companyName} onChange={handleChange} placeholder="Company Name" className="form-control" required />
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col">
                                <label style={{ fontWeight: 'bold' }}> Official Email</label>
                                <input type="email" name="email" value={state.email} onChange={handleChange} placeholder="Official Email" className="form-control" required />
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col">
                                <label style={{ fontWeight: 'bold' }}>Date of Incorporation</label>
                                <input type="date" name="doi" value={state.doi} onChange={handleChange} placeholder="Date of Incorporation" className="form-control" required />
                            </div>
                        </div> */}

                        <div className="row mt-3">
                            <div className="col">
                                <label style={{ fontWeight: 'bold' }}>Select A Plan</label>
                                <select name="plan" className="form-control text-small mb-3" onChange={onChangePlan} required>
                                    <option value="">Select a Plan</option>
                                    <option value="PLN_z8b73tpi05mgk2l/500000">Quarterly - 5,000</option>
                                    <option value="PLN_xsnie2ztlia72ur/1000000">Biannually - 10,000</option>
                                    <option value="PLN_d1in959rixio9um/1500000">Annually - 15,000 </option>
                                </select>

                                {/* <label style={{ fontWeight: 'bold' }}>Was this service introduce to you by a business development advisor as a feature of the enterprise growth boost and development support program?</label>
                                <select className="form-control text-small mb-3" onChange={onChangeAdvisor}>
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>

                                {
                                    showAdvisor ?
                                        <div>
                                            <label style={{fontWeight: 'bold'}}>Advisor Identification Code:</label>
                                            <input type="text" className="form-control text-small" onChange={onChangeAIC} placeholder="Enter advisor's identificaltion code..."></input>
                                        </div>
                                    : ''
                                } */}

                            </div>
                        </div>
                    </div>

                    <div className="row mt-3">
                        <div className="col">
                            <button
                                type="submit"
                                className="btn font-weight-light btn-primary mt-3 py-2 w-100 border-0"
                                disabled={Btn}
                                onClick={showStaff}

                            >
                                {spinner ? <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" /> : "Proceed"}
                            </button>
                        </div>
                    </div>

                </Modal.Body>

            </Modal>
            <PayStackButton
                show={payModal}
                onHide={() => setPayModal(false)}
                close={close}
                callback={onSuccess}
                email={user ? user.email : ""}
                amount={total}
            />

            <StaffModal
                show={staffModal}
                onHide={() => setStaffModal(!staffModal)}
                amount={amount}
                closeStaffModal={closeStaffModal}
            // aic={aic}
            />
        </>
    );
}
export default LMSubForm;