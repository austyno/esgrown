import React from 'react';
import { Modal } from 'react-bootstrap';
import PaystackButton from 'react-paystack';
import './paystack.css';



const PayStackButton = (props) => {

    const { show, onHide, close, callback, email, amount, plan } = props

    return (
        <React.Fragment>
            <Modal
                show={show}
                onHide={onHide}
                aria-labelledby="example-custom-modal-styling-title"
                centered
            >
                <Modal.Body style={{ width: 'auto', background: '#21a5e7' }}>
                    <PaystackButton
                        text="Proceed To Payment"
                        class="pay-btn"
                        close={close}
                        embed={false}
                        callback={callback}
                        email={email}
                        amount={amount}
                        plan={plan}
                        paystackkey={process.env.REACT_APP_PAYSTACK}
                        tag="button"
                    />
                </Modal.Body>


            </Modal>
        </React.Fragment>
    );

}

export default PayStackButton;