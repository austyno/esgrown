import React from 'react';
import SideBar from '../../sidebar';
import '../../../App';
import AppendNavHeader from '../../reusable/append_nav_header';


export default function Account() {

  return (
    <>
    <div >
      <AppendNavHeader title=""/>
          <div className="row" style={{ marginTop: '0px'}}>
            <div className="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12 col-12 sid" style={{marginLeft: '0px', marginRight: '0px'}}>
                {/* <div className=""> */}
              <div class="jumbotron image" style={{height:'60%', marginLeft:'15px'}}>
                <div className="d-flex flex-column justify-content-between p-4" >
                  <div className="">
                    <h1 class="" style={{ fontFamily: 'quicksand', color: 'white', fontSize: '25' }}>Welcome to Esgrown</h1>
                    <hr />
                  </div>
                  <div className=" overlay p-3" style={{marginTop:'0px'}}>
                    <p class="lead d-flex flex-direction-column align-items-center" style={{ color: 'white', fontSize: '20px', fontFamily: 'quicksand', textAlign: 'left' }}>We inspire and empower you to live your highest dreams in the contexts of learning, <br /> productivity and pursuit of success and economic welfare.</p>
                  </div>
                </div>
              </div>
              {/* </div> */}
            </div>
            <div className="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12">
                <SideBar />
            </div>
            </div>
        </div>
    </>
  );
}
