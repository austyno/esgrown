import React, { useEffect, useState } from 'react';
import '../../../../App';
import CompetenceImg from '../../../../img/personnel_competence.jpg';
import TeacherCompetenceImg from '../../../../img/teacher_performance.jpg';




const CompetenceMgt = () => {

    const [user, setUser] = useState()

    useEffect(() => {
        const user = JSON.parse(sessionStorage.getItem('key'));
        setUser(user)

    }, []);
    const image = user && user.tic != null ? TeacherCompetenceImg : CompetenceImg;
    return (
        <div>
            <div >
                <img src={image} alt="" height="50%" width="100%" />
            </div>
        </div>
    );

}

export default CompetenceMgt;