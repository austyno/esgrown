import React from 'react';
import { Card } from 'react-bootstrap';
import EFAImage from '../../../../img/corporate_leadership.jpg';
import '../../../../App';


const LMCard = () => {

    return (
        <div className="">
            <img src={EFAImage} alt="" height="50%" width="100%" />
        </div>
    );
}

export default LMCard;