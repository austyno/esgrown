import React, { useState, useEffect } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import LMVideoCard from './videocards/lmcard';
import LMService from './service-cards/lmservice';
import AppendNavHeader from '../../reusable/append_nav_header';



const LMServicePage = ({sublm}) => {

    const [pageLM, setpageLM] = useState(0);

    useEffect(() => {
        //get logged in user details
        const user = JSON.parse(sessionStorage.getItem('key'));

        if (user.sub_status_lm === 'active') {
            setpageLM(1)
        } else {
            setpageLM(2)
        }

    }, []);

    return (
        // <div className="container">
        //     <div className="row">
        //         <div className="col col-lg-10 col-sm-10">
        //             {(pageLM === 1) ? <LMService /> : (pageLM === 2) ? <LMVideoCard /> : ""}
        //         </div>
        //     </div>
        // </div>

        <div>
            <AppendNavHeader title="Leadership Management Service" />
            <div className="container-fluid" style={{ height: '' }}>
                <div className="d-flex justify-content-center">
                    <div className="row" style={{ width: '100%', height: '100%' }}>
                        <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 col" style={{ padding: "0px" }}><div className="left_col"></div></div>
                        <div className="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-12 center_col" style={{ background: '#f5f5f5', paddingLeft: "30px", paddingRight: "30px", paddingTop: "15px", }}>
                            <div className="d-flex justify-content-center" style={{ height: '100%', width: '100%', }}>
                                {/*  border: '1px solid', borderRightColor:'#E8E6E6', borderLeftColor:'#E8E6E6' */}
                                {/* Inner Div */}
                                <div className="col">
                                    <div>
                                        {(pageLM === 1) ? <LMService /> : (pageLM === 2) ? <LMVideoCard /> : ""}
                                        <div className="row">
                                            <div className="col" style={{ width: '100%' }}>
                                                <button
                                                    className="btn font-weight-bold btn-primary mt-2 w-100 border-0"
                                                    style={{ background: '#1C8496' }}
                                                    onClick={() => sublm()}
                                                >
                                                    Subcribe !
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="d-flex justify-content-center"><button style={{ height: '30px', background: '#1C8496', border: '#1C8496', color: 'white' }}>Subcribe</button></div> */}
                        </div>
                        <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1 col" style={{ padding: "0px" }}><div className="right_col"></div></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LMServicePage;
