import React, { useState, useEffect, Fragment } from 'react';
import PayStackButton from '../paystack/paystackpaymentbutton';
import { Modal, Spinner } from 'react-bootstrap';
import axios from 'axios';
import toast from '../../../util/toast';




export default function Form(props) {

    const [modalShow, setModalShow] = useState(false);
    const [modalPayShow, setModalPayShow] = useState(false);
    const [LOE, setLOE] = useState("");
    const [FIS, setFIS] = useState("");
    const [TIC, setTIC] = useState("");
    const [ticDisplay, setTicDisplay] = useState('none');
    const [useremail, setUserEmail] = useState("");
    const [userId, setUserId] = useState("");
    const [subStatus, setSubStatus] = useState(false);
    const [user, setUser] = useState();
    const [disabled,setDisabled] = useState(false)


    const [subPlan, setSubPlan] = useState("");
    const [amount, setAmount] = useState("");
    const [button, setButton] = useState(2)

    //SPINNER STATE
    const [spinner, setSpinner] = useState(false);

    //GET LOGGEDIN USER CREDENTIALS FOR PMT
    useEffect(() => {
        const userData = JSON.parse(sessionStorage.getItem('key'));
        const userEmail = userData.email;
        const userId = userData.id;
        setUserEmail(userEmail);
        setUserId(userId);
        setUser(userData)


        if (userData.sub_status_eas === 'active') {
            setSubStatus(userData.sub_status_eas);
            setButton(1);

        } else {
            setButton(2);
        }

    }, [])


    const onChangeLOE = (e) => {
        setLOE(e.target.value);

    }

    const onChangeFIS = (e) => {
        setFIS(e.target.value);
    }

    const onRefferalChange = (e) => {
        if (e.target.value == "yes") {
            setTicDisplay('inline-block');
        } else if (e.target.value == "no") {
            setTicDisplay('none');
        }

    }

    const onChangeTIC = (e) => {
        setTIC(e.target.value);
    }

    const onChangePlan = (e) => {
        const plan = e.target.value.split('/');
        setAmount(plan[1]);
        setSubPlan(plan[0]);
    }


    const onSubmit = (e) => {
        e.preventDefault();


        if (LOE == '') {
            toast("Level of Education is required",'error')
            return
        }
        if (FIS === '') {
            toast("Field of intended study is required",'error')
            return
        }
        if (subPlan === '' || amount === '') {
            toast("Please choose a subscription plan",'error');
            return
        }


            const SubObject = {
                user_id: props.user.id,
                sub_status: "inactive",
                user_email: props.user.email,
                user_name: props.user.name,
                user_status: props.user.status,
                levelofeducation: LOE,
                field_of_intended_study: FIS,
                tic: TIC
            }

            try {
                setSpinner(true);
                axios.post(`${process.env.REACT_APP_API_ENDPOINT}/subscriptioneas/add`, SubObject, { headers: { 'authorization': `Bearer ${user.token}` } })
                    .then((res) => {
                        setSpinner(false);
                        setModalShow(false);
                        setModalPayShow(true);
                    })
                    .catch((err) => {
                        console.log('Err: ' + err);
                        setSpinner(false);
                        setModalShow(false);
                    });

            } catch (error) {
                console.log(error);
            }
        }


    //Paystack Functions
    const onSuccess = (res) => {
        console.log(res);
        setModalPayShow(false);

        //make a call to https://api.paystack.co/subscription and get all subscription 
        //and filter for email of current subscription and extract subscription code.
        //update individual doc with the gotten data.

        axios.get(`https://api.paystack.co/subscription`, { headers: { "Authorization": "Bearer sk_test_19f4c12e4e018a9f742e1723d42c9c8e509800b4" } })
            .then(res => {
                const client = res.data.data.filter(st => {
                    return st.customer.email === useremail
                })

                const data = {
                    ref: res.reference,
                    sub_status: client[0].status,
                    sub_code: client[0].subscription_code
                }

                //update eas substatus
                axios.post(`${process.env.REACT_APP_API_ENDPOINT}/subscriptioneas/update/easref/${userId}`, data, { headers: { 'authorization': `Bearer ${user.token}` } })
                    .then(res => console.log(res.data))
                    .catch(err => console.log(err))


                //update user details
                axios.post(`${process.env.REACT_APP_API_ENDPOINT}/individuals/update/substatus/${userId}`, { sub_status_eas: client[0].status }, { headers: { 'authorization': `Bearer ${user.token}` } })
                    .then(res => {
                        const globalUser = JSON.parse(sessionStorage.getItem('key'));

                        globalUser.sub_status_eas = client[0].status;

                        sessionStorage.setItem('key', JSON.stringify(globalUser));
                        setButton(1)

                    }).catch(err => console.log(err))

            }).catch(err => console.log(err));


    }

    const close = () => {
        console.log("Payment closed");
        setModalPayShow(false)
    }

    //display field of study fieldOfStudy
    const displayIntendedFieldOfStudy = () => {
        props.fieldOfStudy.map(item => {
            return (
                <option value={item.field}>{item.field}</option>
            );
        })

    }


    return (
        <React.Fragment>
            <div className="ml-auto d-flex align-items-center">
                <React.Fragment>
                    {(button === 1) ? <button className="btn btn-info btn-sm" disabled style={{ color: 'white', background: '#97ba0d', border: '#97ba0d' }}>Subscribed</button> : (button === 2) ? <button className="btn btn-info btn-sm" onClick={() => setModalShow(true)}>Subscribe</button> : <button className="btn btn-info btn-sm" onClick={() => setModalShow(true)}>Subscribe</button>}
                </React.Fragment>

            </div>
            <Modal
                show={modalShow}
                onHide={() => setModalShow(false)}
                aria-labelledby="example-custom-modal-styling-title"
                centered
            >
                <Modal.Body>

                    <form className="container py-4" onSubmit={onSubmit}>
                        <div className="row">
                            <div className="col">
                                <h6>Education Advisory Subscription</h6>
                            </div>
                        </div>
                        {/* Level of Education */}
                        {/* “Early Childhood Learning”, “Primary/Elementary”, “Junior Secondary” “Senior Secondary”, “Undergraduate”, “Masters” */}
                        <div className="row mt-3">
                            <div className="col">
                                <select className="form-control" required onChange={onChangeLOE}>
                                    <option>Level of Education</option>
                                    <option value="early_childhood_learning">Early Childhood Learning</option>
                                    <option value="primary">Primary/Elementary</option>
                                    <option value="junior_secondary">Junior Secondary</option>
                                    <option value="senior_secondary">Senior Secondary</option>
                                    <option value="undergraduate">Undergraduate</option>
                                    <option value="masters">Masters</option>
                                </select>
                            </div>
                        </div>

                        {/* Intended Study field */}
                        <div className="row mt-3">
                            <div className="col">
                                <select className="form-control" required onChange={onChangeFIS}>
                                    <option>Field of Intended Study</option>
                                    {displayIntendedFieldOfStudy}
                                </select>
                                {/* <input
                                    label=""
                                    type="text"
                                    value={FIS}
                                    onChange={onChangeFIS}
                                    placeholder="Field of Intended Study"
                                    required
                                    className="form-control"
                                /> */}
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col">
                                <select className="form-control" required onChange={onChangePlan}>
                                    <option>Select Subscription Plan</option>
                                    <option value="PLN_euav5svesnpj2ct/1600000">Annual(One Year) @ N16,0000</option>
                                    <option value="PLN_nl0qpw3j7hsi626/900000">Biannual(6 Months) @ N9,000</option>
                                    <option value="PLN_8cowxwb52yeq6th/500000">Quarterly(3 Months) @ N5,000</option>
                                </select>
                            </div>
                        </div>


                        <div className="row mt-3">
                            <div className="col">
                                <select className="form-control" onChange={onRefferalChange}>
                                    <option>Where you refered by a Teacher?</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>

                        {/* Tic field */}
                        <div className="row mt-3" style={{ display: ticDisplay }}>
                            <div className="col">
                                <input
                                    label=""
                                    type="text"
                                    value={TIC}
                                    onChange={onChangeTIC}
                                    placeholder=""
                                    className="form-control"
                                />
                            </div>
                            <div className="ml-3" style={{ opacity: '0.7' }}>Please enter Teacher's Identification Code</div>
                        </div>

                        {/* submit button */}
                        <div className="row mt-3">
                            <div className="col">
                                <button
                                    type="submit"
                                    className="btn font-weight-light btn-primary mt-3 py-2 w-100 border-0"
                                    disabled={spinner}
                                >
                                    Proceed {spinner ?
                                        <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" /> : ""}
                                </button>
                            </div>
                        </div>
                    </form>

                </Modal.Body>
            </Modal>
            <PayStackButton
                show={modalPayShow}
                onHide={() => setModalPayShow(false)}
                close={close}
                callback={onSuccess}
                email={useremail}
                amount={amount}
                plan={subPlan}
            />
        </React.Fragment>
    );
}